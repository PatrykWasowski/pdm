// przed odpaleniem skryptu, trzeba jeszcze wywolac npm install mongodb w danym katalogu

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://192.168.5.11:27017/PDM";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  
  db.collection("products").count.then(function(numItems) {
    console.log("Liczba dokumentów w bazie: " + numItems);
    db.close();
  });
});