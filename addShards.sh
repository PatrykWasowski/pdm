#!/bin/bash
# pierwszy argument- numer shard'a, który ma zostać dołączony 

clusterMembers=$("./getClusterMembers.sh")

shardNumber=$1
shard='({ addShard: "shards'

if [[ $shardNumber -gt 7 ]] || [[ $clusterMembers = *$shardNumber* ]]; then
	echo "Podano zły numer shard'a"
	exit 1
fi

if [[ $shardNumber -eq 1 ]]; then
	shard=$shard'ReplSet/shard'$shardNumber
else
	shard=$shard$shardNumber'ReplSet/shard'$shardNumber
fi

if [[ $shardNumber -ge 6 ]]; then 
	shard=$shard':28017" })'
else
	shard=$shard':27017" })'
fi

command=$(cat adminCommand.js)
command+=$shard

echo "$command" > lastCommand.js

./dropPdmDatabase.sh $shardNumber	# przed dodaniem shard'a, należy się upewnić, że nie miał on jeszcze swojej starej lokalnej wersji bazy PDM

./runCommands.sh lastCommand.js