#!/bin/bash

# pierwszy argument - liczba procesów, które mają zostać uruchomione jednocześnie
# drugi argument - liczba pytań, jaką ma wykonać każdy z procesów

numberOfProcesses=$1
numberOfQueriesPerProcess=$2

numberOfShards="$(./getShardsNumber.sh)"
now="$(date +"%T")"

mongostatFileName="Stats/stats"${numberOfShards}"_"$now".txt"
runSubmittersTimesFilename="Stats/submitting"${numberOfShards}"_"$now".txt"

./gatherMongostatInfo.sh > $mongostatFileName & ./runSubmitters.sh $numberOfProcesses $numberOfQueriesPerProcess 2> $runSubmittersTimesFilename

kill -9 $!
pkill -f "mongostat"

echo "Koniec"


