args = process.argv;
var queriesNumber = args[args.indexOf('-qn') + 1];   // number of queries that will be performed

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://192.168.5.11:27017/PDM";
var queryNumbers = [11, 12, 21, 22, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47]

MongoClient.connect(url, function(err, db) {
	if(err) throw err;
	var caida = db.collection("caida");

	drawAndPerformQueries(caida, queriesNumber);

	db.close();
});

function drawAndPerformQueries(collection, numberOfQueries) {
	array = [];	

	for(i = 0; i < numberOfQueries; ++i) {	
		array[i] = queryNumbers[Math.floor((Math.random() * queryNumbers.length))];
	}

	queryDatabase(collection, array);
}

function queryDatabase(collection, arrayOfQueries) {
	for (queryNumber in arrayOfQueries) {
		performSingleQuery(collection, arrayOfQueries[queryNumber]);
	}
}

function performSingleQuery(collection, queryNumber) {
	switch(queryNumber) {
		case 11:		
			collection.insert({id: "11", name: "test", surname: "wstawienia", _source: { layers: {frame: { time: "Jan 22, 2016, 13:03:000000000 Eur", protocols: "rawrr"}}}}, function(err, res){if(err) throw err;}); 
		break;

		case 12:
			collection.insertMany([{id: "12", name: "test", surname: "wstawienia", _source: { layers: {frame: { time: "Jan 22, 2016, 13:03:000000000 Eur", protocols: "rawrr"}}}}, {id: "12", name: "test", surname: "wstawienia", _source: { layers: {frame: { time: "Jan 20, 2016, 13:07:000000000 Eur", protocols: "rawrr"}}}}, {id: "12", name: "test", surname: "wstawienia", _source: { layers: {frame: { time: "Jan 20, 2016, 13:13:000000000 Eur", protocols: "rawrr"}}}}], function(err, res) { if (err) throw err; }); 
		break;

		case 21:
			collection.deleteMany({id: "11"}, function(err, res) { if (err) throw err; });	
		break;

		case 22:
			collection.deleteMany({id: "12"}, function(err, res) { if (err) throw err; });	
		break;

		case 31:		
			collection.count(function(err, res) { if (err) throw err; console.log(res);}); 
		break;		
		
		case 32:
			collection.count({"_source.layers.frame.protocols": "raw:ip:tcp"}, function(err, res) { if (err) throw err; });
		break;

		case 33:
			collection.count({"_source.layers.frame.time": {$gte: "Jan 21, 2016 13:20:00.000000000 Eur", $lte: "Jan 21, 2016 13:40:00.000000000 Eur"}}, function(err, res) { if (err) throw err; });
		break;

		case 34:
			collection.distinct("_source.layers.frame.protocols", function(err, res) { if (err) throw err; });
		break;

		case 35:
			collection.distinct("_source.layers.ip.src", function(err, res) { if (err) throw err; });
		break;

		case 36:
			collection.distinct("_source.layers.ip.dst", function(err, res) { if (err) throw err; });
		break;

		case 37:
			collection.find({"_source.layers.ip.src": "122.6.236.170"}, function(err, res) { if (err) throw err; });
		break;

		case 38:
			collection.find({"_source.layers.ip.dst": "111.205.228.222"}, function(err, res) { if (err) throw err; });
		break;

		case 39:	
			collection.findOne({}, {"sort": [["_source.layers.frame.length", "desc"]]}, function(err, res) { if (err) throw err; }); // metoda max() wymaga indeksu na zadanym polu, dlatego jej nie użyłem	
		break;		
	
		case 40:
			collection.find().sort({"_source.layers.frame.len": +1}).limit(1); 
		break;		

		case 41:
			collection.find({"_source.layers.frame.time": {$gte: "Jan 21, 2016 13:40:00.000000000 Eur", $lte: "Jan 21, 2016 13:44:00.000000000 Eur"}}, function(err, res) { if (err) throw err; });
		break;

		case 42:
			collection.find({"_source.layers.frame.protocols": "raw:ip:udp:dns"}, function(err, res) { if (err) throw err; });
		break;		

		case 43:
			collection.find({"_source.layers.ip.ttl": {$lte: "20"}}, function(err, res) { if (err) throw err; });
		break;

		case 44:
			collection.find({"_source.layers.tcp.dstport": "443"}, function(err, res) { if (err) throw err; });
		break;

		case 45:
			collection.find({"_source.layers.tcp.srcport": "22"}, function(err, res) { if (err) throw err; });
		break;

		case 46:
			collection.find({"_source.layers.udp.length": {$lte: "50"}}, function(err, res) { if (err) throw err; });
		break;

		case 47:
			collection.findOne({_id: "5a2c3bde99e997bf6366c50c"}, function(err, res) { if (err) throw err; });
		break;
	}

}