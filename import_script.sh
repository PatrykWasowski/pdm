#!/bin/bash
# pierwszy parametr - ścieżka do katalogu, 
# drugi parametr - nazwa kolekcji
FILES="$1*"
FILES_CAT="$1"
COLLECTION="$2"
i=0
files_number="$(ls -1q $FILES_CAT | wc -l)"

echo "Start importing $files_number files"

for f in $FILES
do
	echo "$f"	
	mongoimport --host 192.168.5.11 --port 27017 --db PDM -c $COLLECTION --file $f
	((i++))
	echo "Imported $i of $files_number documents"	
done

echo "Finished importing files"