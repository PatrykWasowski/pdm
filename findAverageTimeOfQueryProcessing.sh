#!/bin/bash
# pierwszy argument - czas próbkowania wyrażony w sekundach

AVERAGE_NUMBER_OF_IDLE_COMMANDS_PER_SHARD=4	# średnia liczba komend, jakie są wysyłane między shardami, kiedy nie wykonywane są żadne zapytania

sampleTime=$1
((sampleTime++))

./gatherMongostatOnlyAboutQueries.sh $sampleTime > lastMongostat.txt

sed -i '/RTR/d' lastMongostat.txt		# usunięcie wszystkich wierszy dotyczących mongos'a
sed -i '/no data/d' lastMongostat.txt		# usunięcie wierszy zawierających info o niemożliwości pobrania statystyk - wiersze te zawierają numery portów, które mogą mieć wpływ na wynik

shardsNumber=$("./getShardsNumber.sh")
sum=$(grep -o '[[:digit:]]*' lastMongostat.txt | paste -sd+ | bc)
result=$(echo "scale = 2; "$sum"/"$shardsNumber | bc)

echo $result