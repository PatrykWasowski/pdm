#!/bin/bash
# pierwszy argument- numer shard'a, który ma zostać odłączony 

shardNumber=$1
shard='({ removeShard: "shards'

if [[ $shardNumber -gt 7 ]]; then
	exit 1
fi

if [[ $shardNumber -eq 1 ]]; then
	shard=$shard'ReplSet'
else
	shard=$shard$shardNumber'ReplSet'
fi

shard=$shard'" })'

command=$(cat adminCommand.js)
command+=$shard

echo "$command"
echo "$command" > lastCommand.js

runCommandCounter=0
finishedSuccessfully=false

while [[ $runCommandCounter -lt 20 ]] && [[ "$finishedSuccessfully" = false ]]
do
	response=$(./runCommands.sh lastCommand.js) 

	if [[ "$response" = *"removeshard completed successfully"* ]]; then
		finishedSuccessfully=true	
	fi

	((runCommandCounter++))
	echo $runCommandCounter
	sleep 5
done

if [[ "$finishedSuccessfully" = true ]]; then 
	echo "Udało się"
	./runCommands.sh lastCommand.js		# żeby być pewnym, że już nie ma sharda w klastrze
	./dropPdmDatabase.sh $shardNumber
fi