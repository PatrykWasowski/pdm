#!/bin/bash
# pierwszy argument jest numer shard'a, z którego zostanie usunięta baza PDM

if [[ $1 -gt 5 ]]; then
	port=28017
	hostNumber=$[$1-5]
else 
	port=27017
	hostNumber=$[$1+2]
fi

host="192.168.5.1"$hostNumber

echo $host":"$port


mongo --host $host --port $port < dropDatabase.js