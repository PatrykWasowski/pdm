#!/bin/bash

shardsList="$(./runCommands.sh clusterMembers.js | grep "_id" )"
result=""

for (( i=1; i<8; i++ )) 
do
	searchedShard="shards"	
	if [ $i -ne 1 ]; then
		searchedShard="$searchedShard$i"
	fi

	if [[ $shardsList = *$searchedShard* ]]; then		
		result+=$i
	fi
done

echo $result
