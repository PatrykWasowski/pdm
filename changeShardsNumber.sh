#!/bin/bash
# pierwszy argument - liczba shard'ów, które mają być dodane lub usunięte - w zależności od znaku (+/-) - do klastra

numberOfShards=$1
echo $numberOfShards

addShards=false

if [[ $numberOfShards -lt 0 ]]; then
	addShards=false
else
	addShards=true
fi

echo $addShards
absoluteValue=${numberOfShards#-}

echo $absoluteValue

currentNumberOfShards=$("./getShardsNumber.sh")

if [[ $((currentNumberOfShards+numberOfShards)) -gt 7 || $((currentNumberOfShards+numberOfShards)) -lt 1 ]]; then
	echo "Podano złą liczbę shard'ów"
	exit 1
fi

currentClusterMembers=$("./getClusterMembers.sh")
if [[ "$addShards" = true ]]; then 
	for(( i=0; i<$absoluteValue; i++ ))
	do
		for (( j=1; j<8; j++))
		do
			if [[ $currentClusterMembers != *$j* ]]; then
				./addShard.sh $j
				break
			fi		
		done 
		currentClusterMembers=$("./getClusterMembers.sh")
	done
else
	for(( i=0; i<$absoluteValue; i++ ))
	do
		for (( j=7; j>0; j--))
		do
			if [[ $currentClusterMembers = *$j* ]]; then
				./removeShard.sh $j
				break
			fi		
		done 
		currentClusterMembers=$("./getClusterMembers.sh")
	done
fi