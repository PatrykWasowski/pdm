#!/bin/bash
# pierwszy argument - liczba procesów, które mają zostać uruchomione jednocześnie
# drugi argument - liczba pytań, jaką ma wykonać każdy z procesów

numberOfProcesses=$1
numberOfQueriesPerProcess=$2

echo "Uruchomiono "$numberOfProcesses" procesów"
echo "Każdy wykonuje "$numberOfQueriesPerProcess" zapytań"

for (( i=0; i<numberOfProcesses; i++ ))
do
	time /usr/bin/node /root/Observer/Submitter.js -qn $numberOfQueriesPerProcess &
	processes[$i]=$!
done

for process in ${processes[*]}
do
	wait $process
done

echo ""

echo "Procesy się skończyły"