#!/bin/bash
# pierwszy parametr - ściezka do katalogu z plikami źródłowymi,
# drugi parametr - ściezka katalogu docelowego, gdzie będą zapisane utworzone JSONy
# trzeci parametr - maksymalna liczba pakietów wyciągana z danego pliku PCAP
# w cygwin'ie należy podać ścieżkę jak: G:/caida_equinix, bo nie działa /cygwin/g/caida_equinix 
INPUT_PATH="$1/*"
OUTPUT_PATH="$2"
MAXIMUM_PACKETS_PER_FILE=$3
counter=1

for f in $INPUT_PATH
do
    if ! [[ -f $f ]]; then
        echo "$f został pominięty"
        continue
    fi
    OUT=$(echo $f | rev | cut -c 8- | rev | echo "$(cat -)json")
    tshark -T json -r $f > $OUT -c $MAXIMUM_PACKETS_PER_FILE    # jeśli uruchamiamy na cygwin'ie to należy wcześniej ręcznie wystartować service wireshark'a komendą 'sc start npf' w normalnej konsoli i podstawić ./tshark na początku tej linii
    echo "$counter - utworzono plik $OUT"
    ((counter++)) 
done
